from traffic_signal import TrafficSignal

# rewards

def dwt_reward(ts: TrafficSignal):
    total_waiting_time = get_total_waiting_time(ts) 
    reward = ts.last_measure - total_waiting_time
    ts.last_measure = total_waiting_time
    
    return reward

def dwt_out_queued_cars_reward(ts: TrafficSignal):
    total_waiting_time = get_total_waiting_time(ts)
    reward = ts.last_measure - total_waiting_time
    ts.last_measure = total_waiting_time

    vehicle_size_min_gap = 7.5  # 5(vehSize) + 2.5(minGap)

    out_queued_cars = [min(1, ts.sumo.lane.getLastStepHaltingNumber(lane) / (ts.sumo.lane.getLength(lane) / vehicle_size_min_gap)) for lane in ts.out_lanes]
    for density in out_queued_cars:
        reward = reward - (50 * density**3)

    return reward

def avg_speed_reward(ts: TrafficSignal):
    return get_normalized_avg_speed(ts)

def avg_wt_reward(ts: TrafficSignal):
    avg_wt = 0.0
    num_veh = 0

    for lane in ts.lanes:
        veh_list = ts.sumo.lane.getLastStepVehicleIDs(lane)
        num_veh += len(veh_list)
        for veh in veh_list:
            avg_wt += ts.sumo.vehicle.getAccumulatedWaitingTime(veh)

    if num_veh > 0:
        avg_wt = avg_wt / num_veh

    reward = ts.last_measure - avg_wt
    ts.last_measure = avg_wt

    return reward

# utils functions

def get_total_waiting_time(ts: TrafficSignal):
    incoming_roads = ["E2TL", "N2TL", "W2TL", "S2TL"]
    car_list = ts.sumo.vehicle.getIDList()
    total_waiting_time = 0.0
    for car_id in car_list:
        wait_time = ts.sumo.vehicle.getAccumulatedWaitingTime(car_id)
        road_id = ts.sumo.vehicle.getRoadID(car_id)  # get the road id where the car is located
        if road_id in incoming_roads:  # consider only the waiting times of cars in incoming roads
           total_waiting_time += wait_time

    return total_waiting_time

def get_normalized_avg_speed(ts: TrafficSignal):
    avg_speed = 0.0
    vehs = ts._get_veh_list()
    if len(vehs) == 0:
        return 1.0
    for v in vehs:
        avg_speed += ts.sumo.vehicle.getSpeed(v) / ts.sumo.vehicle.getAllowedSpeed(v)
    return avg_speed / len(vehs)
