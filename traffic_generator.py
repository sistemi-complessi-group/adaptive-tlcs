import numpy as np
import math
import scipy.stats
import uuid

class TrafficGenerator:
    """
    Generate a route file (.rou.xml) containing traffic generation info based on the selected scenario and configuration.
    """
    def __init__(self, max_steps, n_cars_generated, route_folder: str, artificial_queue=False, scenario="std"):
        self._n_cars_generated = n_cars_generated  # how many cars per episode
        self._max_steps = max_steps # maximum steps in one episode
        self._scenario = scenario # scenario type
        self._queue = artificial_queue # whether to use artificial queue or not
        self._route_folder = route_folder 

    def generate_routefile(self, seed):
        """
        Generation of the route of every car for one episode
        """
        np.random.seed(seed)  # make tests reproducible

        # the generation of cars is distributed according to a weibull distribution
        timings = np.random.weibull(2, self._n_cars_generated)
        timings = np.sort(timings)

        # reshape the distribution to fit the interval 0:max_steps
        car_gen_steps = []
        min_old = math.floor(timings[1])
        max_old = math.ceil(timings[-1])
        min_new = 0
        max_new = self._max_steps
        for value in timings:
            car_gen_steps = np.append(car_gen_steps, ((max_new - min_new) / (max_old - min_old)) * (value - max_old) + max_new)

        car_gen_steps = np.rint(car_gen_steps)  # round every value to int -> effective steps when a car will be generated

        #Probability distribution for artificial queues
        mean = 2000
        standard_deviation = 1500
        y_values = scipy.stats.norm(mean, standard_deviation)

        route_file_name = self._route_folder + "/" + str(uuid.uuid4()) + ".rou.xml"

        # produce the file for cars generation, one car per line
        with open(route_file_name, "w") as routes:
            print("""<routes>
            <vType accel="1.0" decel="4.5" id="standard_car" length="5.0" minGap="2.5" maxSpeed="25" sigma="0.5" />

            <route id="W_N" edges="W2TL TL2N"/>
            <route id="W_E" edges="W2TL TL2E"/>
            <route id="W_S" edges="W2TL TL2S"/>
            <route id="N_W" edges="N2TL TL2W"/>
            <route id="N_E" edges="N2TL TL2E"/>
            <route id="N_S" edges="N2TL TL2S"/>
            <route id="E_W" edges="E2TL TL2W"/>
            <route id="E_N" edges="E2TL TL2N"/>
            <route id="E_S" edges="E2TL TL2S"/>
            <route id="S_W" edges="S2TL TL2W"/>
            <route id="S_N" edges="S2TL TL2N"/>
            <route id="S_E" edges="S2TL TL2E"/>""", file=routes)
            
            #Determine the coming percentage depending on the scenario -> EW 90% - 10% (inverse of NS)
            if (self._scenario == 'EW'):
                coming_from_percentage = 0.90
                if self._queue:
                    factor_artificial_queue_ew = 0.5
                    factor_artificial_queue_ns = 1
                else:
                    factor_artificial_queue_ew = -1 #-1 because 0 creates a stop situation
                    factor_artificial_queue_ns = -1
            elif (self._scenario== 'NS'):
                coming_from_percentage = 0.10
                if self._queue:
                    factor_artificial_queue_ew = 1
                    factor_artificial_queue_ns = 0.5
                else:
                    factor_artificial_queue_ew = -1
                    factor_artificial_queue_ns = -1
            else:
                if self._queue:
                    factor_artificial_queue_ew = 0.5
                    factor_artificial_queue_ns = 0.5
                else:
                    factor_artificial_queue_ew = -1
                    factor_artificial_queue_ns = -1


            #Generate the routes for each car
            for car_counter, step in enumerate(car_gen_steps):

                #Determine the outcoming lane and the waiting time
                random_out_lane = np.random.randint(0,4)
                artificial_queue_ew = factor_artificial_queue_ew * (y_values.pdf(car_gen_steps)[car_counter]*100000)
                artificial_queue_ns = factor_artificial_queue_ns * (y_values.pdf(car_gen_steps)[car_counter]*100000)
                
                #EW or NS scenario
                if(self._scenario == 'EW' or self._scenario == 'NS'):
                    
                    axis_direction = np.random.uniform()
                    #Straight or turn
                    straight_or_turn = np.random.uniform()
                    route_straight = np.random.randint(1, 3)
                    route_turn = np.random.randint(1, 5)
                
                    #EW
                    if axis_direction < coming_from_percentage : #90% coming from the North or South arm for NS scenario or 10% for EW scenario
                        if straight_or_turn < 0.75:
                            if route_straight == 1:
                                start, end = 'E', 'W'
                                queue_duration = artificial_queue_ew
                            elif route_straight == 2:
                                start, end = 'W', 'E'
                                queue_duration = artificial_queue_ew
                        else:
                            if route_turn == 1:
                                start, end = 'W', 'N'
                                queue_duration = artificial_queue_ns
                            elif route_turn == 2:
                                start, end = 'W', 'S'
                                queue_duration = artificial_queue_ns
                            elif route_turn == 3:
                                start, end = 'E', 'N'
                                queue_duration = artificial_queue_ns
                            elif route_turn == 4:
                                start, end = 'E', 'S'
                                queue_duration = artificial_queue_ns
                    #NS
                    else: # the remaining ones
                        if straight_or_turn < 0.75:
                            if route_straight == 1:
                                start, end ='N', 'S'
                                queue_duration = artificial_queue_ns
                            elif route_straight == 2:
                                start, end ='S','N'
                                queue_duration = artificial_queue_ns
                        else:
                            if route_turn == 1:
                                start, end ='S','W'
                                queue_duration = artificial_queue_ew
                            elif route_turn == 2:
                                start, end = 'S', 'E'
                                queue_duration = artificial_queue_ew
                            elif route_turn == 3:
                                start, end ='N', 'W'
                                queue_duration = artificial_queue_ew
                            elif route_turn == 4:
                                start, end ='N', 'E'
                                queue_duration = artificial_queue_ew
                # Low or High scenario 
                else :
                    straight_or_turn = np.random.uniform()
                    if straight_or_turn < 0.75:  # choose direction: straight or turn - 75% of times the car goes straight
                        route_straight = np.random.randint(1, 5)  # choose a random source & destination
                        if route_straight == 1:
                            start, end ='W', 'E'
                            queue_duration = artificial_queue_ew
                        elif route_straight == 2:
                            start, end ='E', 'W'
                            queue_duration = artificial_queue_ew
                        elif route_straight == 3:
                            start, end ='N', 'S'
                            queue_duration = artificial_queue_ns
                        else:
                            start, end ='S','N'
                            queue_duration = artificial_queue_ns
                    else:  # car that turn -25% of the time the car turns
                        route_turn = np.random.randint(1, 9) # choose a random source & destination
                        if route_turn == 1:
                            start, end ='W','N'
                            queue_duration = artificial_queue_ns
                        elif route_turn == 2:
                            start, end ='W', 'S'
                            queue_duration = artificial_queue_ns
                        elif route_turn == 3:
                            start, end ='N','W'
                            queue_duration = artificial_queue_ew
                        elif route_turn == 4:
                            start, end ='N','E'
                            queue_duration = artificial_queue_ew
                        elif route_turn == 5:
                            start, end ='E','N'
                            queue_duration = artificial_queue_ns
                        elif route_turn == 6:
                            start, end ='E','S'
                            queue_duration = artificial_queue_ns
                        elif route_turn == 7:
                            start, end ='S', 'W'
                            queue_duration = artificial_queue_ew
                        elif route_turn == 8:
                            start, end ='S', 'E'
                            queue_duration = artificial_queue_ew

                opening = '    <vehicle id="{start}_{end}_{i}" type="standard_car" route="{start}_{end}" depart="{depart}" departLane="random" departSpeed="10" arrivalLane="{arr_lane}">'.format(
                    start=start, end=end, i=car_counter, depart=step, arr_lane = random_out_lane
                )
                stop = ''
                if self._queue:
                    stop = '<stop lane="TL2{end}_{arr_lane}" endPos="750" duration="{stop_duration}"/>'.format(end=end, arr_lane = random_out_lane, stop_duration=queue_duration)
                closing = '</vehicle>'
                print(opening + stop + closing, file=routes)

            print("</routes>", file=routes)

        return route_file_name

