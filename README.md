# Traffic Light Control Signal

### Requisiti
* Eclipse SUMO (download dal [sito ufficiale](https://sumo.dlr.de/docs/Downloads.php))
* Anaconda 4 ([sito ufficiale](https://www.anaconda.com/))

### Creazione dell'ambiente 
1. Creazione di un ambiente virtuale:
    ```bash
    conda create -n tlcs python=3.9
    ```

2. Attivazione dell'ambiente
    ```bash
    conda activate tlcs
    ```

3. Installazione di PyTorch
    ```bash
    # utilizzo con solo CPU, Linux e Windows
    conda install pytorch torchvision torchaudio cpuonly -c pytorch

    # utilizzo con GPU, Linux e Windows
    conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch

    # default, MacOS
    conda install pytorch torchvision torchaudio -c pytorch
    ```

4. Installazione degli altri _packages_
    ```bash
    pip3 install stable-baselines3==1.6.2
    pip3 install gym==0.25.1
    pip3 install libsumo
    pip3 install pettingzoo
    pip3 install scipy
    ```

### Esecuzione degli script
Per addestrare un modello, è sufficiente inserire la configurazione desiderata nel file `train_settings.json` ed eseguire il relativo script:
```bash
# LIBSUMO_AS_TRACI è una variabile d'ambiente che consente di velocizzare di circa 8 volte 
# l'esecuzione delle simulazioni in assenza di GUI. Può essere impostata separatamente 
# oppure (con Linux e MacOS) in-line nel comando.

# Linux e MaxOS
LIBSUMO_AS_TRACI=1 python train.py

# Windows
python train.py
```

Il modello generato verrà salvato in `models/{model_name}.zip`, dove `model_name` è il nome del modello inserito nel file di configurazione. Inoltre, è possibile visualizzare i log dell'addestramento in `train_logs/{model_name}/`.

Per eseguire delle simulazioni di test e valutare un modello, è sufficiente specificare la configurazione desiderata in `test_settings.json` (tra cui  `model_name` e `simulation_name`) ed eseguire lo script. 
```bash
# simulazioni senza GUI, Linux e MacOS
LIBSUMO_AS_TRACI=1 python test.py

# simulazioni con GUI e/o con Windows
python test.py

```
I grafici prodotti verranno salvati in `plots/{model_name}/{simulation_name}/`. Utilizzare `model_name=fixed` e `static_traffic_lights=True` nei `test_settings.json` se si vuole testare il semaforo standard.

Sia per l'esecuzione dello script di training, sia per l'esecuzione dello script di test, è necessario specificare nel codice quale reward function utilizzare, passandola come parametro nella dichiarazione dell'environment Gym.

I file _.rou.xml_ generati per ogni simulazione vengono salvati in `intersection/`, sebbene siano esclusi dal tracking di Git.
