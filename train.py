from env import SumoEnvironment
from stable_baselines3 import DQN
from stable_baselines3.common.vec_env import SubprocVecEnv
from traffic_generator import TrafficGenerator
from visualization import Visualization
import reward_fns as rf
from numpy import load
import json
import os
import shutil

# number of scenarios simulated concurrently for each training iteration
numScenarios = 8

# estimated average length of an episode (n of actions taken by the agent)
sim_steps = 500

# read configuration
config = json.load(open("train_settings.json"))
max_steps = config['max_steps']
delta_time = config['delta_time']
model_name = config['model_name']

# total timesteps of the training: avg number of steps * desired number of iterations * n of scenarios per iteration
tot_timesteps = sim_steps * config['total_episodes'] * numScenarios 

# function for generating an environment with the correct configuration
def envGenerator(trafficGenerator: TrafficGenerator):
        return SumoEnvironment(
            net_file = config['net_file_path'],
            route = trafficGenerator,
            num_seconds = max_steps,
            delta_time = delta_time,
            yellow_time = config['yellow_duration'],
            min_green = config['green_duration'],
            single_agent = True,
            reward_fn= rf.dwt_reward,
            sumo_warnings = False,
            obs_out_lanes = config['obs_out_lanes'],
            allred_phase = config['allred_phase'],
        )

# function for plotting the avg rewards during training evaluations
def plotRewards(config):
    path = config['train_logs_dir'] + config['model_name']
    data = load(path + '/evaluations.npz')
    rewards = [r[0] for r in data['results']]

    visualization = Visualization(
        path, 
        dpi=96
    )

    visualization.save_data_x_y_and_plot(
        data_x = [x * 2 for x in range(1, len(rewards) + 1)],
        data_y=rewards,
        filename='train_rewards',
        title='Rewards during model "' + config['model_name'] + '" training',
        xlabel = 'Episode',
        ylabel='Reward'
    )

# main function
if __name__ == '__main__':
    # set and create training route file folder
    route_folder = "intersection/train_route"
    os.makedirs(route_folder, exist_ok=True)

    # create a different traffic generator for each different scenario
    generator1 = TrafficGenerator(max_steps, config['n_cars_generated_high'], route_folder, False, 'std')
    generator2 = TrafficGenerator(max_steps, config['n_cars_generated_low'], route_folder, False, 'std')
    generator3 = TrafficGenerator(max_steps, config['n_cars_generated_ew'], route_folder, False, 'EW')
    generator4 = TrafficGenerator(max_steps, config['n_cars_generated_ns'], route_folder, False, 'NS')
    generator5 = TrafficGenerator(max_steps, config['n_cars_generated_high'], route_folder, True, 'std')
    generator6 = TrafficGenerator(max_steps, config['n_cars_generated_low'], route_folder, True, 'std')
    generator7 = TrafficGenerator(max_steps, config['n_cars_generated_ew'], route_folder, True, 'EW')
    generator8 = TrafficGenerator(max_steps, config['n_cars_generated_ns'], route_folder, True, 'NS')

    # define a gym vectorized environment with an environment for each different scenario
    vecEnv = SubprocVecEnv(env_fns=[lambda :envGenerator(trafficGenerator=generator1),
                                    lambda :envGenerator(trafficGenerator=generator2),
                                    lambda :envGenerator(trafficGenerator=generator3),
                                    lambda :envGenerator(trafficGenerator=generator4),
                                    lambda :envGenerator(trafficGenerator=generator5),
                                    lambda :envGenerator(trafficGenerator=generator6),
                                    lambda :envGenerator(trafficGenerator=generator7),
                                    lambda :envGenerator(trafficGenerator=generator8)])

    # reset the environment for initialization
    vecEnv.reset()

    # neural network configuration
    policy_kwargs = dict(net_arch = [config['width_layers']] * config['num_layers'])
    model = DQN(policy="MlpPolicy",
        env = vecEnv,
        gamma = config['gamma'],
        learning_rate = config['learning_rate'],
        learning_starts = sim_steps * numScenarios,
        batch_size = config['batch_size'],
        buffer_size = config['memory_size_max'],
        exploration_initial_eps = config['exp_initial_eps'],
        exploration_final_eps = config['exp_final_eps'],
        exploration_fraction = config['exp_fraction'],
        policy_kwargs = policy_kwargs,
        train_freq=1,
        verbose = 1,
    )

    # test environment used for training evalutation (low traffic scenario)
    testEnv = envGenerator(generator2)

    # training of the NN
    model.learn(
        total_timesteps = tot_timesteps,
        eval_env=testEnv,
        eval_freq = sim_steps * 2, # evaluation is done every two iterations (more or less)
        n_eval_episodes=1, # the evaluation of the model is made using just one episode
        eval_log_path=config['train_logs_dir'] + model_name + '/',
    )

    # close the environment
    vecEnv.close()

    # save the trained model
    model.save(config['model_path'] + model_name)

    # create rewards plot and save them
    plotRewards(config)
    
    # delete the route files created for training
    shutil.rmtree(route_folder)