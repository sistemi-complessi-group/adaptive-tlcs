import os
import sys
from typing import Callable, List, Union
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("Please declare the environment variable 'SUMO_HOME'")
import traci
import numpy as np
from gym import spaces


class TrafficSignal:
    """
    This class represents a Traffic Signal of an intersection
    It is responsible for retrieving information and changing the traffic phase using Traci API
    IMPORTANT: It assumes that the traffic phases defined in the .net file are of the form:
        [green_phase, yellow_phase, green_phase, yellow_phase, ...]
    You can change the observation space this by modifing self.observation_space and the method _compute_observations()
    Action space is which green phase is going to be open until the next action.
    """
    def __init__(self, 
                env,
                ts_id: List[str],
                delta_time: int, 
                yellow_time: int, 
                min_green: int, 
                max_green: int,
                begin_time: int,
                reward_fn: Callable,
                obs_out_lanes: bool,
                allred_phase: bool,
                sumo):
        # set internal configuration
        self.id = ts_id
        self.env = env
        self.delta_time = delta_time
        self.yellow_time = yellow_time
        self.min_green = min_green
        self.max_green = max_green
        self.reward_fn = reward_fn
        self.allred_phase = allred_phase
        self.sumo = sumo

        # initialize internal values
        self.green_phase = 0 # current green phase
        self.is_yellow = False # if True, the TL is currently yellow
        self.time_since_last_phase_change = 0 # seconds passed since the last phase change
        self.next_action_time = begin_time # time when the next agent action should take place
        self.last_measure = 0.0 # last measure collected (used by reward functions)
        self.last_reward = None # last reward collected

        # build the TL phases using the .net file
        self.build_phases()

        # contains only incoming lanes
        self.lanes = list(dict.fromkeys(self.sumo.trafficlight.getControlledLanes(self.id)))  # Remove duplicates and keep order

        # list of outgoing lanes
        self.out_lanes = [link[0][1] for link in self.sumo.trafficlight.getControlledLinks(self.id) if link]
        self.out_lanes = list(set(self.out_lanes))
        
        # length of each lane
        self.lanes_lenght = {lane: self.sumo.lane.getLength(lane) for lane in self.lanes + self.out_lanes}

        # list of observed lanes (could be all lanes or only incoming lanes)
        self.observed_lanes = list()
        if obs_out_lanes:
            self.observed_lanes = self.lanes + self.out_lanes
        else:
            self.observed_lanes = self.lanes

        # definition of the observation and action spaces
        self.observation_space = spaces.Box(
            low=np.zeros(self.num_green_phases+2*len(self.observed_lanes), dtype=np.float32), 
            high=np.ones(self.num_green_phases+2*len(self.observed_lanes), dtype=np.float32)
        )
       
        self.action_space = spaces.Discrete(self.num_green_phases)

    def build_phases(self):
        # get all phases as defined in the .net file
        phases = self.sumo.trafficlight.getAllProgramLogics(self.id)[0].phases

        # if using the fixed TL, use the classic sumo logic. Count only the green phases (not yellow) as possible actions.
        if self.env.fixed_ts:
            self.num_green_phases = len(phases)//2  # Number of green phases == number of phases (green+yellow) divided by 2
            return

        # otherwise, select only green phases (for action space) and then calculate all yellow-transition
        # between different phases

        self.green_phases = []
        self.yellow_dict = {}
        for phase in phases: # for each phase, if not yellow add it to green phases
            state = phase.state
            if 'y' not in state and (state.count('r') + state.count('s') != len(state)): # if not yellow
                self.green_phases.append(self.sumo.trafficlight.Phase(60, state))

        # if settings contains allred_phase=True , add the red phase to the green phases as a possible action
        if self.allred_phase:
            self.green_phases.append(self.sumo.trafficlight.Phase(5, 'rrrrrrrrrrrrrrrrrrrr'))
        
        self.num_green_phases = len(self.green_phases)
        self.all_phases = self.green_phases.copy()

        # create yellow phases for transitions between all couples of green phases
        for i, p1 in enumerate(self.green_phases):
            for j, p2 in enumerate(self.green_phases):
                if i == j: continue
                yellow_state = ''
                for s in range(len(p1.state)):
                    if (p1.state[s] == 'G' or p1.state[s] == 'g') and (p2.state[s] == 'r' or p2.state[s] == 's'):
                        yellow_state += 'y'
                    else:
                        yellow_state += p1.state[s]
                self.yellow_dict[(i,j)] = len(self.all_phases)
                self.all_phases.append(self.sumo.trafficlight.Phase(self.yellow_time, yellow_state))

        programs = self.sumo.trafficlight.getAllProgramLogics(self.id)
        logic = programs[0]
        logic.type = 0
        logic.phases = self.all_phases

        # set the new TL program logic in sumo
        self.sumo.trafficlight.setProgramLogic(self.id, logic)
        self.sumo.trafficlight.setRedYellowGreenState(self.id, self.all_phases[0].state)

    @property
    def time_to_act(self):
        """
        Returns True if the current time of the simulation equal "next_action_time".
        """
        return self.next_action_time == self.env.sim_step
    
    def update(self):
        """
        Update by one second the TL state. Eventually turn off the yellow phase.
        """
        self.time_since_last_phase_change += 1
        if self.is_yellow and self.time_since_last_phase_change == self.yellow_time:
            self.sumo.trafficlight.setRedYellowGreenState(self.id, self.all_phases[self.green_phase].state)
            self.is_yellow = False

    def set_next_phase(self, new_phase):
        """
        Sets what will be the next green phase (eventually a yellow phase).
        :param new_phase: (int) Number between [0..num_green_phases] 
        """

        new_phase = int(new_phase)

        # if phase didn't change, next action is in delta_time seconds
        if self.green_phase == new_phase: 
            self.sumo.trafficlight.setRedYellowGreenState(self.id, self.all_phases[self.green_phase].state)
            self.next_action_time = self.env.sim_step + self.delta_time
        elif self.green_phase != 4:
             # if phase changes and the current is not AllRed, activate the yellow phase and prepare the following (selected) phase.
             # Then define the next action time. 

            # set yellow phase
            self.sumo.trafficlight.setRedYellowGreenState(self.id, self.all_phases[self.yellow_dict[(self.green_phase, new_phase)]].state)

            # set the next green phase, transition will be made automatically after yellow_time seconds
            self.green_phase = new_phase

            if new_phase == 4: # if next is red
                self.next_action_time = self.env.sim_step + self.delta_time # delta time is more than yellow duration and red has no min duration
            else:
                self.next_action_time = self.env.sim_step + max(self.delta_time, self.min_green + self.yellow_time)

            self.is_yellow = True
            self.time_since_last_phase_change = 0
        else: # if now is all red phase and phase is changed, skip yellow
            self.sumo.trafficlight.setRedYellowGreenState(self.id, self.all_phases[new_phase].state)
            self.green_phase = new_phase
            self.next_action_time = self.env.sim_step + self.min_green
            self.time_since_last_phase_change = 0

    def compute_observation(self):
        phase_id = [1 if self.green_phase == i else 0 for i in range(self.num_green_phases)]  # one-hot encoding
        density = self.get_obs_lanes_density()
        queue = self.get_obs_lanes_queue()

        observation = np.array(phase_id + density + queue, dtype=np.float32)
        return observation
            
    def compute_reward(self):
        """
        Compute the reward using the provided reward function.
        """
        self.last_reward = self.reward_fn(self)
        return self.last_reward
    
    def update_vehicles_wt(self):
        """
        Update the environment internal representation of the time spent waiting (in incoming lanes) by each single vehicle.
        """
        for lane in self.lanes:
            veh_list = self.sumo.lane.getLastStepVehicleIDs(lane)
            for veh in veh_list:
                acc = self.sumo.vehicle.getAccumulatedWaitingTime(veh)
                self.env.vehicles_wt[veh] = acc
                
    def get_obs_lanes_density(self):
        vehicle_size_min_gap = 7.5  # 5(vehSize) + 2.5(minGap)
        return [min(1, self.sumo.lane.getLastStepVehicleNumber(lane) / (self.lanes_lenght[lane] / vehicle_size_min_gap)) for lane in self.observed_lanes]

    def get_obs_lanes_queue(self):
        vehicle_size_min_gap = 7.5  # 5(vehSize) + 2.5(minGap)
        return [min(1, self.sumo.lane.getLastStepHaltingNumber(lane) / (self.lanes_lenght[lane] / vehicle_size_min_gap)) for lane in self.observed_lanes]
    
    def get_total_queued(self):
        """
        Get total number of queued vehicles in incoming lanes.
        """
        tot = 0
        for lane in self.lanes:
            tot = tot + self.sumo.lane.getLastStepHaltingNumber(lane)

        return tot

    def _get_veh_list(self):
        """
        Get list of vehicles in incoming lanes.
        """
        veh_list = []
        for lane in self.lanes:
            veh_list += self.sumo.lane.getLastStepVehicleIDs(lane)
        return veh_list