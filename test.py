import numpy as np
from env import SumoEnvironment
from stable_baselines3 import DQN
from traffic_generator import TrafficGenerator
from visualization import Visualization
import json
import os
import shutil
import reward_fns as rf

# main function
if __name__ == '__main__':

    # read settings
    config = json.load(open("test_settings.json"))
    max_steps = config['max_steps']
    fixed = config['static_traffic_lights']
    num_episodes = config['num_episodes']
    delta_time = config['delta_time']

    # define path where results will be saved ( plots/{model_name}/{simulation_name} )
    plots_model_dir = os.getcwd() + "/plots/" + config['model_name']
    plots_sim_dir = plots_model_dir + '/' + config['simulation_name']
    os.makedirs(plots_model_dir, exist_ok=True)
    os.makedirs(plots_sim_dir, exist_ok=True)

    # create visualization object used for creating and saving the plots
    visualization = Visualization(
        plots_sim_dir, 
        dpi=96
    )

    # define path of the test route files
    route_folder = "intersection/test_routes"
    os.makedirs(route_folder, exist_ok=True)

    # function for creating the environment with the correct configuration
    def get_env(n):
        test_route_file = "intersection/test_route" + str(n) + ".rou.xml"
        generator = TrafficGenerator(max_steps, config['n_cars_generated'], route_folder, config['art_queues'], config['scenario'])
        return SumoEnvironment(
            net_file = config['net_file_path'],
            route = generator,
            use_gui = config['gui'],
            num_seconds = max_steps,
            delta_time = delta_time,
            yellow_time = config['yellow_duration'],
            min_green = config['green_duration'],
            single_agent = not fixed,
            reward_fn= rf.avg_wt_reward,
            fixed_ts = fixed,
            sumo_warnings = False,
            obs_out_lanes = config['obs_out_lanes'],
            allred_phase = config['allred_phase'],
        )


    # if the simulation doesn't use fixed TL, load the desired model
    if not fixed:
        model = DQN.load(config['model_path'] + config['model_name'])

    # initialize stats
    total_queued_cars = [0 for i in range(max_steps)]
    vehicles_wt = []
    chosen_red = [0 for i in range(num_episodes)]

    # for each episode, run a complete simulation and update stats
    for i in range(num_episodes):
        env = get_env(i)
        obs = env.reset()
        info = {}
        done = False

        # until the simulation is not finished
        while not done:
            # if fixed TL, no action is taken, otherwise let the model predict a new action based on the current observations
            if fixed:
                action = None
            else:
                action = model.predict(obs, deterministic=True)[0]

            # run a simulation step with the chosen action and get back new observations and reward
            obs, reward, done, info = env.step(action)
            
            # if red phase is active, check if it has been chosen and update stats
            if config['allred_phase']:
                if action == 4: # 4 is the code of allred phase
                    chosen_red[i] += 1

        # update stats
        ep_queued_cars = env.get_queued_cars_per_second()
        if len(ep_queued_cars) > len(total_queued_cars):
            total_queued_cars = total_queued_cars + [0 for i in range(len(ep_queued_cars) - len(total_queued_cars))]
        total_queued_cars = [total_queued_cars[j] + ep_queued_cars[j] for j in range(len(ep_queued_cars))]

        vehicles_wt = vehicles_wt + info["vehicles_wt"]
        env.close()
    
    # Metrics evaluation and plots
    total_queued_cars = np.array(total_queued_cars) / num_episodes

    metricsOutput = "Avg waiting time: " + str(sum(vehicles_wt) / len(vehicles_wt)) + "\n"
    metricsOutput += "Max wt: " + str(max(vehicles_wt)) + "\n"

    if config['allred_phase']:
        metricsOutput += 'Avg all red phase usage: ' + str(sum(chosen_red) / num_episodes) + " times"

    print(metricsOutput)

    shutil.rmtree(route_folder)

    # create plots and save results if necessary
    if config['save_results']:
        with open(os.path.join(plots_sim_dir + '/metrics.txt'), 'w') as file:
            file.write(metricsOutput)

        visualization.save_data_x_y_and_plot(
            data_x = [x for x in range(1, len(total_queued_cars) + 1)],
            data_y=total_queued_cars, 
            filename="total_queued_cars", 
            title="Queue length average during testing", 
            xlabel='Seconds', 
            ylabel='Queue length average (vehicles)'
        )

        visualization.histogram_plot(
            data=vehicles_wt,
            filename="vehicles_wt_hist",
            title="Vehicles wt",
            xlabel="Seconds waited",
            ylabel="N of vehicles",
            bins=25
        )
