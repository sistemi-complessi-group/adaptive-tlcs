import configparser

# Read the config file regarding the training and import its content
def import_train_configuration(config_file):
    content = configparser.ConfigParser()
    content.read(config_file)
    config = {}

    # simulation
    config['gui'] = content['simulation'].getboolean('gui')
    config['total_episodes'] = content['simulation'].getint('total_episodes')
    config['max_steps'] = content['simulation'].getint('max_steps')
    config['n_cars_generated_high'] = content['simulation'].getint('n_cars_generated_high')
    config['n_cars_generated_low'] = content['simulation'].getint('n_cars_generated_low')
    config['n_cars_generated_ew'] = content['simulation'].getint('n_cars_generated_ew')
    config['n_cars_generated_ns'] = content['simulation'].getint('n_cars_generated_ns')
    config['green_duration'] = content['simulation'].getint('green_duration')
    config['yellow_duration'] = content['simulation'].getint('yellow_duration')
    config['delta_time'] = content['simulation'].getint('delta_time')
    config['obs_out_lanes'] = content['simulation'].getboolean('obs_out_lanes')
    config['allred_phase'] = content['simulation'].getboolean('allred_phase')

    # model
    config['num_layers'] = content['model'].getint('num_layers')
    config['width_layers'] = content['model'].getint('width_layers')
    config['batch_size'] = content['model'].getint('batch_size')
    config['learning_rate'] = content['model'].getfloat('learning_rate')
    config['gamma'] = content['model'].getfloat('gamma')
    config['exp_initial_eps'] = content['model'].getfloat('exp_initial_eps')
    config['exp_final_eps'] = content['model'].getfloat('exp_final_eps')
    config['exp_fraction'] = content['model'].getfloat('exp_fraction')

    # memory
    config['memory_size_max'] = content['memory'].getint('memory_size_max')
    
    # dir
    config['models_path_name'] = content['dir']['models_path_name']
    config['net_file_path'] = content['dir']['net_file_path']
    config['model_name'] = content['dir']['model_name']
    config['train_logs_dir'] = content['dir']['train_logs_dir']

    return config

# Read the config file regarding the testing and import its content
def import_test_configuration(config_file):
    content = configparser.ConfigParser()
    content.read(config_file)
    config = {}

    # simulation
    config['simulation_name'] = content['simulation']['simulation_name']
    config['gui'] = content['simulation'].getboolean('gui')
    config['max_steps'] = content['simulation'].getint('max_steps')
    config['n_cars_generated'] = content['simulation'].getint('n_cars_generated')
    config['scenario'] = content['simulation'].get('scenario')
    config['episode_seed'] = content['simulation'].getint('episode_seed')
    config['yellow_duration'] = content['simulation'].getint('yellow_duration')
    config['green_duration'] = content['simulation'].getint('green_duration')
    config['delta_time'] = content['simulation'].getint('delta_time')
    config['art_queues'] = content['simulation'].getboolean('art_queues')
    config['static_traffic_lights'] = content['simulation'].getboolean('static_traffic_lights')
    config['num_episodes'] = content['simulation'].getint('num_episodes')
    config['obs_out_lanes'] = content['simulation'].getboolean('obs_out_lanes')
    config['allred_phase'] = content['simulation'].getboolean('allred_phase')
    config['save_results'] = content['simulation'].getboolean('save_results')

    # dir
    config['model_path'] = content['dir']['model_path']
    config['model_name'] = content['dir']['model_name']
    config['net_file_path'] = content['dir']['net_file_path']
    
    return config