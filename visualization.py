import matplotlib.pyplot as plt
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"

class Visualization:
    """
    Utility class for creating and saving plots.
    :param path: (str) the folder where plots should be saved
    :param dpi: (int) resolution of the plots
    """
    def __init__(self, path, dpi):
            self._path = path
            self._dpi = dpi

    def save_data_and_plot(self, data, filename, title, xlabel, ylabel):
        """
        Produce a plot of performance of the agent over the session and save the relative data to txt
        """
        min_val = min(data)
        max_val = max(data)

        plt.rcParams.update({'font.size': 24})  # set bigger font size

        plt.title(title)
        plt.plot(data)
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.margins(0)
        plt.ylim(min_val - 0.05 * abs(min_val), max_val + 0.05 * abs(max_val))
        fig = plt.gcf()
        fig.set_size_inches(20, 11.25)
        fig.savefig(os.path.join(self._path, 'plot_'+ filename+'.png'), dpi=self._dpi)
        plt.close("all")

        with open(os.path.join(self._path, 'plot_'+ filename + '_data.txt'), "w") as file:
            for value in data:
                    file.write("%s\n" % value)


    def histogram_plot(self, data, filename, title, xlabel, ylabel, bins):
        """
        Produce an histogram plot with the received data and save it.
        """
        plt.rcParams.update({'font.size': 24})  # set bigger font size
        max_val = max(data)


        plt.title(title)
        plt.hist(data, bins, ec='white')
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.margins(0)
        # plt.ylim(0, max_val)

        fig = plt.gcf()
        fig.set_size_inches(20, 11.25)
        fig.savefig(os.path.join(self._path, 'plot_'+ filename+'.png'), dpi=self._dpi)
        plt.close("all")


    def save_data_x_y_and_plot(self, data_x, data_y, filename, title, xlabel, ylabel):
        """
        Produce a plot of performance of the agent over the session and save the relative data to txt, with x and y values
        """
        min_val = min(data_y)
        max_val = max(data_y)

        plt.rcParams.update({'font.size': 24})  # set bigger font size

        plt.title(title)
        plt.plot(data_x, data_y)
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.margins(0)
        plt.ylim(min_val - 0.05 * abs(min_val), max_val + 0.05 * abs(max_val))
        fig = plt.gcf()
        fig.set_size_inches(20, 11.25)
        fig.savefig(os.path.join(self._path, 'plot_'+filename+'.png'), dpi=self._dpi)
        plt.close("all")

        with open(os.path.join(self._path, 'plot_'+filename + '_data.txt'), "w") as file:
            for value in data_x:
                    file.write("%s\n" % value)
            for value in data_y:
                    file.write("%s\n" % value)